package at.fhooe.mc.ois.simulator;

import at.fhooe.mc.ois.entity.data.NMEAInfo;

/**
 * The interface, which all observers have to implement
 */
public interface PositionUpdateObserver {
    /**
     * This method is called by the Observable, whenever new data is available
     *
     * @param _info the data, which is available
     */
    void updatePosition(NMEAInfo _info);
}
