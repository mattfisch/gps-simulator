package at.fhooe.mc.ois.simulator;

/**
 * Every Simulator needs to implement this interface
 */
public interface IGPSSimulator {

    /**
     * Reads a line of data from a gps block
     *
     * @return the line read as string
     */
    String readLine();
}
