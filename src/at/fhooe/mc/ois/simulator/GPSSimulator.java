package at.fhooe.mc.ois.simulator;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * A class for simulating GPS data emission
 */
public class GPSSimulator extends BufferedReader implements IGPSSimulator {

    /**
     * the time between simulated GPS data output
     */
    private final long mSleep;

    /**
     * a filter string which signals a new block of GPS data
     */
    private final String mFilter;

    /**
     * Constructor
     *
     * @param _filename the path to the file, which provides the Simulator with GPS data
     * @param _sleep    the time between simulated GPS data output
     * @param _timeUnit the unit of time for which to sleep for
     * @param _filter   a filter string which signals a new block of GPS data
     * @throws FileNotFoundException
     */
    public GPSSimulator(String _filename, int _sleep, TimeUnit _timeUnit, String _filter) throws FileNotFoundException {
        super(new FileReader(_filename));
        mSleep = _timeUnit.toMillis(_sleep);
        mFilter = _filter;
    }

    @Override
    public String readLine() {
        String line = null;
        try {
            line = super.readLine();
        } catch (IOException _e) {
            _e.printStackTrace();
        }
        if (line == null)
            return "eof";
        else {
            if (line.contains(mFilter))
                try {
                    Thread.sleep(mSleep);
                } catch (InterruptedException _e) {
                    _e.printStackTrace();
                }
            return line;
        }
    }
}
