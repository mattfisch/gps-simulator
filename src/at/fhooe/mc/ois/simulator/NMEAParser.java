package at.fhooe.mc.ois.simulator;

import at.fhooe.mc.ois.entity.data.NMEAInfo;
import at.fhooe.mc.ois.entity.data.SatelliteInfo;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The parser, which parses the GPS data string into an object
 */
public class NMEAParser {

    /**
     * The simulator, which provides the parser with raw gps data strings
     */
    private final IGPSSimulator mGPSSimulator;

    /**
     * A list of observers, which will be notified, when a block was read
     */
    private final List<PositionUpdateObserver> mPositionUpdateObserverList;

    /**
     * Constructor
     *
     * @param _gpsSimulator an instance of a simulator, which provides gps data
     */
    public NMEAParser(IGPSSimulator _gpsSimulator) {
        mGPSSimulator = _gpsSimulator;
        mPositionUpdateObserverList = new LinkedList<>();
    }

    /**
     * starts a new thread, reads the data and parses it, then notifying the observers
     */
    public void start() {
        Thread thread = new Thread(() -> {
            String data = "";
            NMEAInfo currentInfo = new NMEAInfo();

            while (!data.equals("eof")) {
                data = mGPSSimulator.readLine();

                if (data.contains("$GPGGA")) {
                    String[] dataArr = data.split(",|\\*");

                    String checkSumStr = "GPGGA,";
                    for (int i = 1; i < dataArr.length - 2; i++) {
                        checkSumStr += dataArr[i] + ",";
                    }
                    checkSumStr += dataArr[dataArr.length - 2];
                    int checkSum = -1;
                    try {
                        checkSum = Integer.parseInt(dataArr[dataArr.length - 1], 16);
                    } catch (NumberFormatException _e) {
                        //foo
                    }

                    if (checkSum != -1 && isValid(checkSumStr, checkSum)) {
                        System.out.println("GPGGA was valid");
                        if (!dataArr[2].isEmpty()) {
                            String lat = dataArr[2];
                            currentInfo.setLatitude(minuteToHourLatLon(2, lat));
                        }
                        if (!dataArr[4].isEmpty()) {
                            String lon = dataArr[4];
                            currentInfo.setLongitude(minuteToHourLatLon(3, lon));
                        }

                        currentInfo.setTimestamp(Float.valueOf(dataArr[1]));

                        if (!dataArr[3].isEmpty())
                            currentInfo.setLatCardinalPt(dataArr[3].charAt(0));

                        if (!dataArr[5].isEmpty())
                            currentInfo.setLonCardinalPt(dataArr[5].charAt(0));

                        switch (Integer.valueOf(dataArr[6])) {
                            case (0):
                                currentInfo.setFixQuality(NMEAInfo.FIX_QUALITY.NO);
                                break;
                            case (1):
                                currentInfo.setFixQuality(NMEAInfo.FIX_QUALITY.GPS);
                                break;
                            case (2):
                                currentInfo.setFixQuality(NMEAInfo.FIX_QUALITY.DIFFERENTIAL);
                                break;
                        }

                        if (!dataArr[8].isEmpty())
                            currentInfo.setHDOP(Double.valueOf(dataArr[8]));

                        if (!dataArr[9].isEmpty())
                            currentInfo.setHeight(Double.valueOf(dataArr[9]));
                    } else {
                        System.out.println("GPGGA was invalid");
                    }
                } else if (data.contains("$GPGSA")) {
                    String[] dataArr = data.split(",|\\*");

                    String checkSumStr = "GPGSA,";
                    for (int i = 1; i < dataArr.length - 2; i++) {
                        checkSumStr += dataArr[i] + ",";
                    }
                    checkSumStr += dataArr[dataArr.length - 2];
                    int checkSum = -1;
                    try {
                        checkSum = Integer.parseInt(dataArr[dataArr.length - 1], 16);
                    } catch (NumberFormatException _e) {

                    }

                    if (checkSum != -1 && isValid(checkSumStr, checkSum)) {
                        System.out.println("GPGSA was valid");
                        if (!dataArr[15].isEmpty())
                            currentInfo.setPDOP(Double.valueOf(dataArr[15]));

                        if (!dataArr[17].isEmpty())
                            currentInfo.setVDOP(Double.valueOf(dataArr[17]));

                        for (int i = 3; i <= 14; i++) {
                            if (!dataArr[i].isEmpty())
                                currentInfo.addSateliteInfo(new SatelliteInfo(Integer.valueOf(dataArr[i]), true));
                        }
                    } else {
                        System.out.println("GPGSA was invalid");
                    }
                } else if (data.contains("$GPGSV")) {
                    String[] dataArr = data.split(",|\\*");

                    String checkSumStr = "GPGSV,";
                    for (int i = 1; i < dataArr.length - 2; i++) {
                        checkSumStr += dataArr[i] + ",";
                    }
                    checkSumStr += dataArr[dataArr.length - 2];
                    int checkSum = -1;
                    try {
                        checkSum = Integer.parseInt(dataArr[dataArr.length - 1], 16);
                    } catch (NumberFormatException _e) {

                    }

                    if (checkSum != -1 && isValid(checkSumStr, checkSum)) {
                        System.out.println("GPGSV was valid");

                        currentInfo.setSateliteSum(Integer.valueOf(dataArr[3]));

                        for (int i = 4; i < dataArr.length - 2; i += 4) {
                            SatelliteInfo satelliteInfo = new SatelliteInfo(Integer.valueOf(dataArr[i]), false);

                            if (!dataArr[i + 1].isEmpty())
                                satelliteInfo.setVerticalAngle(Integer.valueOf(dataArr[i + 1]));

                            if (!dataArr[i + 2].isEmpty())
                                satelliteInfo.setHorizontalAngle(Integer.valueOf(dataArr[i + 2]));

                            if (!dataArr[i + 3].isEmpty())
                                satelliteInfo.setSNR(Integer.valueOf(dataArr[i + 3]));

                            currentInfo.addSateliteInfo(satelliteInfo);
                        }
                    } else {
                        System.out.println("GPGSV was invalid");
                    }

                    if (dataArr[2].equals(dataArr[1])) {
                        notifyObservers(new NMEAInfo(currentInfo));
                        currentInfo = new NMEAInfo();
                    }
                }
            }
        });
        thread.start();
    }

    /**
     * Adds an observer to the list
     *
     * @param _updateObserver the observer, which will be notified
     */
    public void addObserver(PositionUpdateObserver _updateObserver) {
        mPositionUpdateObserverList.add(_updateObserver);
    }

    /**
     * Notifies each observer with the new information
     *
     * @param _info the info object, which will be forwared to the observers
     */
    private void notifyObservers(NMEAInfo _info) {
        mPositionUpdateObserverList.forEach(_updateObserver -> _updateObserver.updatePosition(_info));
    }

    /**
     * converts a decimalminute to a decimalhour
     *
     * @param _splitAt latitude is splitted at position 2, longitude is splitted at position 3
     * @param _val
     * @return
     */
    private double minuteToHourLatLon(int _splitAt, String _val) {
        int hr = Integer.valueOf(_val.substring(0, _splitAt));
        double min = Double.valueOf(_val.substring(_splitAt, _val.length()));
        return hr + (min / 60);
    }

    /**
     * Checks, if the string and its checksum are the same
     *
     * @param _checksumStr the string which is bitwise XOR multiplied
     * @param _checksum    the corresponding checksum
     * @return true, if the string is useable
     */
    private boolean isValid(String _checksumStr, int _checksum) {
        System.out.println(_checksum);
        System.out.println(checkSum(_checksumStr.getBytes()));
        return _checksum == checkSum(_checksumStr.getBytes());
    }

    /**
     * XOR multiplies all bytes in the byte array
     *
     * @param bytes the bytes, which are XOR connected
     * @return the checksum as byte
     */
    public byte checkSum(byte[] bytes) {
        byte sum = 0;
        for (byte b : bytes) {
            sum ^= b;
        }
        return sum;
    }
}
