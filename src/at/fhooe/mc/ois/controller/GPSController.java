package at.fhooe.mc.ois.controller;

import at.fhooe.mc.ois.model.GPSModel;
import at.fhooe.mc.ois.simulator.PositionUpdateObserver;
import at.fhooe.mc.ois.view.GPSView;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * The controller, handling the communication between view and model
 */
public class GPSController implements WindowListener {

    /**
     * The instance to the model
     */
    private final GPSModel mGPSModel;

    /**
     * Constructor
     *
     * @param _GPSModel an instance to a model
     */
    public GPSController(GPSModel _GPSModel) {
        mGPSModel = _GPSModel;
    }

    /**
     * Starts the gps simulation
     */
    public void startGPSSimulation() {
        mGPSModel.startGPSSimulation();
    }

    /**
     * Adds an oberver from the view to the model
     *
     * @param _positionUpdateObserver called, whenever the model has new data for the view available
     */
    public void addObserver(PositionUpdateObserver _positionUpdateObserver) {
        mGPSModel.addObserver(_positionUpdateObserver);
    }

    @Override
    public void windowOpened(WindowEvent _e) {

    }

    @Override
    public void windowClosing(WindowEvent _e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent _e) {

    }

    @Override
    public void windowIconified(WindowEvent _e) {

    }

    @Override
    public void windowDeiconified(WindowEvent _e) {

    }

    @Override
    public void windowActivated(WindowEvent _e) {

    }

    @Override
    public void windowDeactivated(WindowEvent _e) {

    }
}
