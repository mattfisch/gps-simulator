package at.fhooe.mc.ois.matrix;


/**
 * This interface represents all manipulations, which can be done using an implementation of this interface
 */
public interface IMatrix {

    /**
     * Liefert eine String-Repräsentation der Matrix
     *
     * @return Ein String mit dem Inhalt der Matrix
     * @see String
     */
    public String toString();

    /**
     * Liefert die Invers-Matrix der Transformationsmatrix
     *
     * @return Die Invers-Matrix
     */
    public Matrix invers();

    /**
     * Liefert eine Matrix, die das Ergebnis einer Matrizen-
     * multiplikation zwischen dieser und der übergebenen Matrix
     * ist
     *
     * @param _other Die Matrix mit der Multipliziert werden soll
     * @return Die Ergebnismatrix der Multiplikation
     */
    public Matrix multiply(Matrix _other);

    /**
     * Multipliziert einen Punkt mit der Matrix und liefert das
     * Ergebnis der Multiplikation zurück
     *
     * @param _pt Der Punkt, der mit der Matrix multipliziert
     *            werden soll
     * @return Ein neuer Punkt, der das Ergebnis der
     * Multiplikation repräsentiert
     * @see java.awt.Point
     */
    public java.awt.Point multiply(java.awt.Point _pt);
}
