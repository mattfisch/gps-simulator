package at.fhooe.mc.ois.entity.data;

import java.util.LinkedList;
import java.util.List;

/**
 * The object, which will be returned by the parser
 */
public class NMEAInfo {

    /**
     * An enumeration, referencing the quality of the data block
     */
    public enum FIX_QUALITY {
        NO(1),
        GPS(2),
        DIFFERENTIAL(3);

        private final int mCode;

        FIX_QUALITY(int _code) {
            mCode = _code;
        }
    }

    /**
     * The time, when the info was received
     */
    private float mTimestamp;

    /**
     * Latitude in decimal degree
     */
    private double mLatitude;

    /**
     * Char indicating, if North or South
     */
    private char mLatCardinalPt;

    /**
     * Longitude in decimal degree
     */
    private double mLongitude;

    /**
     * Char indicating, if East or West
     */
    private char mLonCardinalPt;

    /**
     * Number of satelites, which are found by the GPS device
     */
    private int mSateliteSum;

    /**
     * Horizontal dilution of position
     */
    private double mHDOP;

    /**
     * Position dilution of positino
     */
    private double mPDOP;

    /**
     * Vertical dilution of position
     */
    private double mVDOP;

    /**
     * Quality of the info block
     */
    private FIX_QUALITY mFixQuality;

    /**
     * Height above sea level
     */
    private double mHeight;

    /**
     * List with detailed info to all satellites
     */
    private final List<SatelliteInfo> mSatelliteInfoList;

    /**
     * Constuctor
     */
    public NMEAInfo() {
        mSatelliteInfoList = new LinkedList<>();
    }

    /**
     * Copy constructor
     *
     * @param _other the other object
     */
    public NMEAInfo(NMEAInfo _other) {
        mTimestamp = _other.getTimestamp();
        mLatitude = _other.getLatitude();
        mLatCardinalPt = _other.getLatCardinalPt();
        mLongitude = _other.getLongitude();
        mLonCardinalPt = _other.getLonCardinalPt();
        mSateliteSum = _other.getSateliteSum();
        mHDOP = _other.getHDOP();
        mPDOP = _other.getPDOP();
        mVDOP = _other.getVDOP();
        mFixQuality = _other.getFixQuality();
        mHeight = _other.getHeight();
        mSatelliteInfoList = new LinkedList<>();
        mSatelliteInfoList.addAll(_other.getSatelliteInfoList());
    }

    /**
     * @return Latitude in decimal degree
     */
    public double getLatitude() {
        return mLatitude;
    }

    /**
     * @param _latitude in decimal degree
     */
    public void setLatitude(double _latitude) {
        mLatitude = _latitude;
    }

    /**
     * @return Longitude in decimal degree
     */
    public double getLongitude() {
        return mLongitude;
    }

    /**
     * @param _longitude in decimal degree
     */
    public void setLongitude(double _longitude) {
        mLongitude = _longitude;
    }

    /**
     * @return Timestamp in milliseconds
     */
    public float getTimestamp() {
        return mTimestamp;
    }

    /**
     * @param _timestamp un milliseconds
     */
    public void setTimestamp(float _timestamp) {
        mTimestamp = _timestamp;
    }

    /**
     * @return Char indicating, if North or South
     */
    public char getLatCardinalPt() {
        return mLatCardinalPt;
    }

    /**
     * @param _latCardinalPt Char indicating, if North or South
     */
    public void setLatCardinalPt(char _latCardinalPt) {
        mLatCardinalPt = _latCardinalPt;
    }

    /**
     * @return Char indicating, if East or West
     */
    public char getLonCardinalPt() {
        return mLonCardinalPt;
    }

    /**
     * @param _lonCardinalPt Char indicating, if East or West
     */
    public void setLonCardinalPt(char _lonCardinalPt) {
        mLonCardinalPt = _lonCardinalPt;
    }

    /**
     * @return number of satellites
     */
    public int getSateliteSum() {
        return mSateliteSum;
    }

    /**
     * @param _sateliteSum number of satellites
     */
    public void setSateliteSum(int _sateliteSum) {
        mSateliteSum = _sateliteSum;
    }

    /**
     * @return Horizontal dilution of precision
     */
    public double getHDOP() {
        return mHDOP;
    }

    /**
     * @param _HDOP Horizontal dilution of precision
     */
    public void setHDOP(double _HDOP) {
        mHDOP = _HDOP;
    }

    /**
     * @return Position dilution of precision
     */
    public double getPDOP() {
        return mPDOP;
    }

    /**
     * @param _PDOP Position dilution of precision
     */
    public void setPDOP(double _PDOP) {
        mPDOP = _PDOP;
    }

    /**
     * @return Vertical dilution of precision
     */
    public double getVDOP() {
        return mVDOP;
    }

    /**
     * @param _VDOP Vertical dilution of precision
     */
    public void setVDOP(double _VDOP) {
        mVDOP = _VDOP;
    }

    /**
     * @return Quality of the data block
     */
    public FIX_QUALITY getFixQuality() {
        return mFixQuality;
    }

    /**
     * @param _fixQuality of the data block
     */
    public void setFixQuality(FIX_QUALITY _fixQuality) {
        mFixQuality = _fixQuality;
    }

    /**
     * @return Height from sea level
     */
    public double getHeight() {
        return mHeight;
    }

    /**
     * @param _height from sea level
     */
    public void setHeight(double _height) {
        mHeight = _height;
    }

    /**
     * Adds a satelite to the list, if the satellite is already in the list, its data is updated
     *
     * @param _satelliteInfo the satellite to add
     */
    public void addSateliteInfo(SatelliteInfo _satelliteInfo) {
        int index = mSatelliteInfoList.indexOf(_satelliteInfo);
        if (index != -1) {
            SatelliteInfo satelliteInfo = mSatelliteInfoList.get(index);
            satelliteInfo.setHorizontalAngle(_satelliteInfo.getHorizontalAngle());
            satelliteInfo.setVerticalAngle(_satelliteInfo.getVerticalAngle());
            satelliteInfo.setSNR(_satelliteInfo.getSNR());
        } else
            mSatelliteInfoList.add(_satelliteInfo);
    }

    /**
     * Returns the list, containing the satellite info
     *
     * @return the satellite info list
     */
    public List<SatelliteInfo> getSatelliteInfoList() {
        return mSatelliteInfoList;
    }

    @Override
    public String toString() {
        return "NMEAInfo{" +
                "Timestamp=" + mTimestamp +
                ", Latitude=" + mLatitude +
                ", LatCardinalPt=" + mLatCardinalPt +
                ", Longitude=" + mLongitude +
                ", LonCardinalPt=" + mLonCardinalPt +
                ", SateliteSum=" + mSateliteSum +
                ", HDOP=" + mHDOP +
                ", PDOP=" + mPDOP +
                ", VDOP=" + mVDOP +
                ", FixQuality=" + mFixQuality +
                ", Height=" + mHeight +
                ", SatelliteInfoList=" + mSatelliteInfoList +
                '}';
    }
}
