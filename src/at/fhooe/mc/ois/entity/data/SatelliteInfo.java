package at.fhooe.mc.ois.entity.data;

/**
 * The satellite info, which will be returned by the parser
 */
public class SatelliteInfo {

    /**
     * The id of the satellite
     */
    private final int mId;

    /**
     * Indicator, if this satellite was used for positioning
     */
    private final boolean mIsInUse;

    /**
     * The horizontal angle in degree between gps receiver and satellite
     */
    private int mHorizontalAngle;

    /**
     * The vertical angle in degree between gps receiver and satellite
     */
    private int mVerticalAngle;

    /**
     * The signal to noise ratio
     */
    private int mSNR;

    /**
     * Constructor
     *
     * @param _id      id of the satellite
     * @param _isInUse whether the satellite was used for positioning or not
     */
    public SatelliteInfo(int _id, boolean _isInUse) {
        mId = _id;
        mIsInUse = _isInUse;
    }

    /**
     * @return id of the satellite
     */
    public int getId() {
        return mId;
    }

    /**
     * @return whether the satellite was used for positioning or not
     */
    public boolean isInUse() {
        return mIsInUse;
    }

    /**
     * @param _horizontalAngle in degree between gps receiver and satellite
     */
    public void setHorizontalAngle(int _horizontalAngle) {
        mHorizontalAngle = _horizontalAngle;
    }

    /**
     * @return horizontal angle in degree between gps receiver and satellite
     */
    public int getHorizontalAngle() {
        return mHorizontalAngle;
    }

    /**
     * @param _verticalAngle in degree between gps receiver and satellite
     */
    public void setVerticalAngle(int _verticalAngle) {
        mVerticalAngle = _verticalAngle;
    }

    /**
     * @return vertical angle in degree between gps receiver and satellite
     */
    public int getVerticalAngle() {
        return mVerticalAngle;
    }

    /**
     * @param _SNR signal to noise ratio
     */
    public void setSNR(int _SNR) {
        mSNR = _SNR;
    }

    /**
     * @return SNR the signal to noise ratio
     */
    public int getSNR() {
        return mSNR;
    }

    @Override
    public String toString() {
        return "SatelliteInfo{" +
                "mId=" + mId +
                ", mIsInUse=" + mIsInUse +
                ", mHorizontalAngle=" + mHorizontalAngle +
                ", mVerticalAngle=" + mVerticalAngle +
                ", mSNR=" + mSNR +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        SatelliteInfo other = (SatelliteInfo) obj;
        return mId == other.getId();
    }
}
