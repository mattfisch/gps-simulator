package at.fhooe.mc.ois.entity.domain;

import at.fhooe.mc.ois.entity.data.SatelliteInfo;

import java.awt.*;

public class Satellite {

    private static final int SATELITE_DIAMETER = 25;

    private final int mId;

    private final boolean mIsInUse;

    private final int mHorizontalAngle;

    private final int mVerticalAngle;

    private final int mSNR;

    public Satellite(SatelliteInfo _satelliteInfo) {
        mId = _satelliteInfo.getId();
        mIsInUse = _satelliteInfo.isInUse();
        mHorizontalAngle = _satelliteInfo.getHorizontalAngle();
        mVerticalAngle = _satelliteInfo.getVerticalAngle();
        mSNR = _satelliteInfo.getSNR();
    }

    public void draw(Graphics _g, int _radius, Point _center) {
        int radius = (int) (_radius * Math.cos(Math.toRadians(mVerticalAngle)));

        int dx = (int) (radius * Math.sin(Math.toRadians(mHorizontalAngle)));
        int dy = (int) (radius * Math.cos(Math.toRadians(mHorizontalAngle)));
        Point sateliteCenter = new Point(_center.x + dx, _center.y - dy);

        if (mIsInUse)
            _g.setColor(Color.GREEN);
        else
            _g.setColor(Color.RED);

        _g.fillOval(sateliteCenter.x - SATELITE_DIAMETER / 2,
                sateliteCenter.y - SATELITE_DIAMETER / 2,
                SATELITE_DIAMETER, SATELITE_DIAMETER);

        _g.drawLine((int) (sateliteCenter.x - SATELITE_DIAMETER * 0.7), sateliteCenter.y,
                (int) (sateliteCenter.x + SATELITE_DIAMETER * 0.7), sateliteCenter.y);

        _g.drawLine((int) (sateliteCenter.x - SATELITE_DIAMETER * 0.7), sateliteCenter.y - 8,
                (int) (sateliteCenter.x - SATELITE_DIAMETER * 0.7), sateliteCenter.y + 8);

        _g.drawLine((int) (sateliteCenter.x + SATELITE_DIAMETER * 0.7), sateliteCenter.y - 8,
                (int) (sateliteCenter.x + SATELITE_DIAMETER * 0.7), sateliteCenter.y + 8);


        _g.setColor(Color.WHITE);
        char[] idChar = String.valueOf(mId).toCharArray();
        _g.drawChars(idChar, 0, idChar.length, sateliteCenter.x - 7, sateliteCenter.y + 5);
    }
}
