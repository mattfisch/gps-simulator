package at.fhooe.mc.ois.entity.domain;

import at.fhooe.mc.ois.entity.data.NMEAInfo;
import at.fhooe.mc.ois.matrix.Matrix;

import java.awt.*;

/**
 * This class represents a gps position received
 */
public class Position {

    /**
     * Close positions are only by 0.000001 different, therefore we need a hight weight factor
     * to take this into account
     */
    private static final int WEIGHT_FACTOR = 1000000;

    /**
     * Latitude of the point, weighted with a factor of 1.000.000
     */
    private final int mLatitude;

    /**
     * Longitude of the point, weighted with a factor of 1.000.000
     */
    private final int mLongitude;

    /**
     * Constructor
     *
     * @param _nmeaInfo the info object from which to extract the position
     */
    public Position(NMEAInfo _nmeaInfo) {
        mLatitude = (int) (_nmeaInfo.getLatitude() * WEIGHT_FACTOR);
        mLongitude = (int) (_nmeaInfo.getLongitude() * WEIGHT_FACTOR);
    }

    /**
     * @return Latitude of the point, weighted with a factor of 1.000.000
     */
    public int getLatitude() {
        return mLatitude;
    }

    /**
     * @return Longitude of the point, weighted with a factor of 1.000.000
     */
    public int getLongitude() {
        return mLongitude;
    }

    /**
     * @param _g      the graphics context, into which to draw the position
     * @param _matrix the matrix, which transforms world into window coordinates
     */
    public void draw(Graphics _g, Matrix _matrix) {
        _g.setColor(Color.RED);
        Point pos = _matrix.multiply(new Point((mLongitude), (mLatitude)));
        _g.fillOval(pos.x, pos.y, 3, 3);
    }
}
