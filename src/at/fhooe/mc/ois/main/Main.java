package at.fhooe.mc.ois.main;

import at.fhooe.mc.ois.controller.GPSController;
import at.fhooe.mc.ois.model.GPSModel;
import at.fhooe.mc.ois.simulator.GPSSimulator;
import at.fhooe.mc.ois.simulator.IGPSSimulator;
import at.fhooe.mc.ois.simulator.NMEAParser;
import at.fhooe.mc.ois.view.GPSView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

/**
 * Main entry point
 */
public class Main {

    /**
     * Runns the program
     *
     * @param args arguments are not supported
     */
    public static void main(String[] args) {

        File gpsLogFile = new File(System.getProperty("user.dir") + "/GPSLogs/GPS-Log-II.log");

        NMEAParser parser = null;

        try {
            IGPSSimulator mGPSSimulator = new GPSSimulator(gpsLogFile.getAbsolutePath(), 1, TimeUnit.SECONDS, "$GPGGA");
            parser = new NMEAParser(mGPSSimulator);
        } catch (FileNotFoundException _e) {
            _e.printStackTrace();
        }

        GPSModel model = new GPSModel(parser);
        GPSController controller = new GPSController(model);
        new GPSView(controller);
    }
}
