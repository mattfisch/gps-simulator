package at.fhooe.mc.ois.model;

import at.fhooe.mc.ois.simulator.NMEAParser;
import at.fhooe.mc.ois.simulator.PositionUpdateObserver;

import java.util.List;

/**
 * Model class, which implements the parser, to provide simulated GPS data
 */
public class GPSModel {

    /**
     * The parser, which uses the simulator to provide GPS data
     */
    private final NMEAParser mNMEAParser;

    /**
     * Constructor
     *
     * @param _NMEAParser an instance of a parser
     */
    public GPSModel(NMEAParser _NMEAParser) {
        mNMEAParser = _NMEAParser;
    }

    /**
     * Starts the parsers thread, which reads in the file with GPS data
     */
    public void startGPSSimulation() {
        mNMEAParser.start();
    }

    /**
     * Adds an observer to the parsers observer list
     *
     * @param _positionUpdateObserver the observer, which will receive GPS data updates
     */
    public void addObserver(PositionUpdateObserver _positionUpdateObserver) {
        mNMEAParser.addObserver(_positionUpdateObserver);
    }
}
