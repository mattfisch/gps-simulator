package at.fhooe.mc.ois.view;

import at.fhooe.mc.ois.controller.GPSController;
import at.fhooe.mc.ois.view.panel.DataPanel;
import at.fhooe.mc.ois.view.panel.DeviationPanel;
import at.fhooe.mc.ois.view.panel.SatellitePanel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The Main application frame, containing all the views
 */
public class GPSView extends Frame {

    /**
     * The initial application window height
     */
    public static final int DEFAULT_HEIGHT = 600;

    /**
     * The initial application window width
     */
    public static final int DEFAULT_WIDHT = 900;

    /**
     * The instance of the applications controller
     */
    private final GPSController mGPSController;

    /**
     * The instance of the applications data panel
     */
    private final DataPanel mDataPanel;

    /**
     * The instance of the applications deviation panel
     */
    private final DeviationPanel mDeviationPanel;

    /**
     * The instance of the applications satellite panel
     */
    private final SatellitePanel mSatellitePanel;

    /**
     * Constructor
     *
     * @param _gpsController the controller of the application
     */
    public GPSView(GPSController _gpsController) {
        super();

        mGPSController = _gpsController;
        mDataPanel = new DataPanel();
        mDeviationPanel = new DeviationPanel();
        mSatellitePanel = new SatellitePanel();

        mGPSController.addObserver(mDataPanel);
        mGPSController.addObserver(mDeviationPanel);
        mGPSController.addObserver(mSatellitePanel);

        addWindowListener(mGPSController);

        setLayout(new BorderLayout());
        setSize(DEFAULT_WIDHT, DEFAULT_HEIGHT);

        Panel mainWindowPanel = new Panel();
        mainWindowPanel.setLayout(new GridLayout(1, 2));

        mainWindowPanel.add(mSatellitePanel);
        mainWindowPanel.add(mDeviationPanel);

        add(mainWindowPanel);
        add(mDataPanel, BorderLayout.SOUTH);

        MenuBar menu = new MenuBar();
        Menu simulation = new Menu("Simulation");
        MenuItem start = new MenuItem("Start");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Clicked");
                mGPSController.startGPSSimulation();
            }
        });
        simulation.add(start);
        menu.add(simulation);
        setMenuBar(menu);

        setVisible(true);
    }
}
