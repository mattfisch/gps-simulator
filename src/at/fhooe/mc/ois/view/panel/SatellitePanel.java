package at.fhooe.mc.ois.view.panel;

import at.fhooe.mc.ois.entity.data.NMEAInfo;
import at.fhooe.mc.ois.entity.domain.Satellite;
import at.fhooe.mc.ois.simulator.PositionUpdateObserver;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

/**
 * The panel, containing the representation of all satellites and their respective position to the gps device used
 */
public class SatellitePanel extends Panel implements PositionUpdateObserver, ComponentListener {

    /**
     * The radius of the outer circle
     */
    private int mRadius;

    /**
     * The diameter of the outer circle
     */
    private int mDiameter;

    /**
     * The satellites, which will be drawn on the screen
     */
    private Vector<Satellite> mSatellites;

    /**
     * Constructor
     */
    public SatellitePanel() {
        super();
        setBackground(Color.WHITE);
        addComponentListener(this);
        mSatellites = new Vector<>();
    }

    @Override
    public void updatePosition(NMEAInfo _info) {
        mSatellites.clear();
        _info.getSatelliteInfoList().forEach(_satelliteInfo -> mSatellites.add(new Satellite(_satelliteInfo)));
        repaint();
    }

    @Override
    public void paint(Graphics _g) {
        super.paint(_g);

        _g.setColor(Color.BLACK);

        int rad45 = (int) (mRadius * Math.cos(Math.toRadians(45)));

        _g.drawOval((getWidth() - rad45 * 2) / 2, (getHeight() - rad45 * 2) / 2, rad45 * 2, rad45 * 2);
        _g.drawOval((getWidth() - mDiameter) / 2, (getHeight() - mDiameter) / 2, mDiameter, mDiameter);

        mSatellites.forEach(_satellite -> _satellite.draw(_g, mRadius, new Point(getWidth() / 2, getHeight() / 2)));
    }

    @Override
    public void componentResized(ComponentEvent _e) {
        int height = (int) _e.getComponent().getSize().getHeight();
        int width = (int) _e.getComponent().getSize().getWidth();

        if (height < width)
            mRadius = (height / 2) - 10;
        else
            mRadius = (width / 2) - 10;

        mDiameter = mRadius * 2;

        repaint();
    }

    @Override
    public void componentMoved(ComponentEvent _e) {

    }

    @Override
    public void componentShown(ComponentEvent _e) {

    }

    @Override
    public void componentHidden(ComponentEvent _e) {

    }
}
