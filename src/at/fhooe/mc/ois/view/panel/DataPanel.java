package at.fhooe.mc.ois.view.panel;

import at.fhooe.mc.ois.entity.data.NMEAInfo;
import at.fhooe.mc.ois.simulator.PositionUpdateObserver;
import at.fhooe.mc.ois.view.GPSView;

import java.awt.*;

/**
 * The panel, showing the data of the simulation
 */
public class DataPanel extends Panel implements PositionUpdateObserver {

    /**
     * A textfield for the Latitude
     */
    private TextField mTxtLatitude;

    /**
     * A textfield for the Longitude
     */
    private TextField mTxtLongitude;

    /**
     * A textfield for the Altitude
     */
    private TextField mTxtAltitude;

    /**
     * A textfield for the FixMode
     */
    private TextField mTxtFixMode;

    /**
     * A textfield for the PDOP
     */
    private TextField mTxtPDOP;

    /**
     * A textfield for the HDOP
     */
    private TextField mTxtHDOP;

    /**
     * A textfield for the VDOP
     */
    private TextField mTxtVDOP;

    /**
     * Constructor
     */
    public DataPanel() {
        super();
        setPreferredSize(new Dimension(GPSView.DEFAULT_WIDHT, 190));
        setLayout(new GridLayout(10, 2));
        Label lblLatitude = new Label("Latitude: ");
        Label lblLongitude = new Label("Longitude: ");
        Label lblAltitude = new Label("Altitude: ");
        Label lblFixMode = new Label("Fix Mode: ");
        Label lblPDOP = new Label("PDOP: ");
        Label lblHDOP = new Label("HDOP: ");
        Label lblVDOP = new Label("VDOP: ");
        Label lblSatelites = new Label("Satelites: ");

        mTxtLatitude = new TextField();
        mTxtLatitude.setEnabled(false);
        mTxtLongitude = new TextField();
        mTxtLongitude.setEnabled(false);
        mTxtAltitude = new TextField();
        mTxtAltitude.setEnabled(false);
        mTxtFixMode = new TextField();
        mTxtFixMode.setEnabled(false);
        mTxtPDOP = new TextField();
        mTxtPDOP.setEnabled(false);
        mTxtHDOP = new TextField();
        mTxtHDOP.setEnabled(false);
        mTxtVDOP = new TextField();
        mTxtVDOP.setEnabled(false);

        add(lblLatitude);
        add(mTxtLatitude);

        add(lblLongitude);
        add(mTxtLongitude);

        add(lblAltitude);
        add(mTxtAltitude);

        add(lblFixMode);
        add(mTxtFixMode);

        add(lblPDOP);
        add(mTxtPDOP);

        add(lblHDOP);
        add(mTxtHDOP);

        add(lblVDOP);
        add(mTxtVDOP);
    }

    @Override
    public void updatePosition(NMEAInfo _info) {
        mTxtLatitude.setText(String.valueOf(_info.getLatitude() + String.valueOf(_info.getLatCardinalPt())));
        mTxtLongitude.setText(String.valueOf(_info.getLongitude() + String.valueOf(_info.getLonCardinalPt())));
        mTxtAltitude.setText(String.valueOf(_info.getHeight()));
        mTxtFixMode.setText(_info.getFixQuality() != null ? _info.getFixQuality().toString() : "");
        mTxtPDOP.setText(String.valueOf(_info.getPDOP()));
        mTxtHDOP.setText(String.valueOf(_info.getHDOP()));
        mTxtVDOP.setText(String.valueOf(_info.getVDOP()));
    }
}
