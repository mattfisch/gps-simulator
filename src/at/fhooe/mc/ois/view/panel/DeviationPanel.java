package at.fhooe.mc.ois.view.panel;

import at.fhooe.mc.ois.entity.data.NMEAInfo;
import at.fhooe.mc.ois.entity.domain.Position;
import at.fhooe.mc.ois.matrix.Matrix;
import at.fhooe.mc.ois.simulator.PositionUpdateObserver;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

/**
 * The panel, showing the deviation of all points on a panel
 */
public class DeviationPanel extends Panel implements PositionUpdateObserver, ComponentListener {

    /**
     * The radius of the outer ring
     */
    private int mRadius;

    /**
     * The diameter of the outer ring
     */
    private int mDiameter;

    /**
     * A vector containing all positions
     */
    private Vector<Position> mCoordPositions;

    /**
     * A transformation matrix for all points
     */
    private Matrix mTransformationMatrix;

    /**
     * Constructor
     */
    public DeviationPanel() {
        super();
        setBackground(Color.WHITE);
        mCoordPositions = new Vector<>();
        addComponentListener(this);
    }

    @Override
    public void updatePosition(NMEAInfo _info) {
        if (_info.getLatitude() != 0 && _info.getLongitude() != 0) {
            mCoordPositions.add(new Position(_info));
            mTransformationMatrix = calculateBoundingBox();
        }
        repaint();
    }

    @Override
    public void paint(Graphics _g) {
        super.paint(_g);

        Graphics2D g2d = (Graphics2D) _g;

        _g.setColor(Color.BLACK);

        int rad25 = (int) (mRadius * Math.cos(Math.toRadians(80)));
        int rad50 = (int) (mRadius * Math.cos(Math.toRadians(60)));
        int rad75 = (int) (mRadius * Math.cos(Math.toRadians(40)));

        g2d.setStroke(new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10, new float[]{10, 10}, 0));
        g2d.drawOval((getWidth() - rad25 * 2) / 2, (getHeight() - rad25 * 2) / 2, rad25 * 2, rad25 * 2);
        g2d.drawOval((getWidth() - rad50 * 2) / 2, (getHeight() - rad50 * 2) / 2, rad50 * 2, rad50 * 2);
        g2d.drawOval((getWidth() - rad75 * 2) / 2, (getHeight() - rad75 * 2) / 2, rad75 * 2, rad75 * 2);
        g2d.drawOval((getWidth() - mDiameter) / 2, (getHeight() - mDiameter) / 2, mDiameter, mDiameter);


        mCoordPositions.forEach(_position -> _position.draw(_g, mTransformationMatrix));
    }

    /**
     * Calculates the bounding box of all positions
     *
     * @return the Matrix, which will place a point inside the bounding box, when multiplied with
     */
    private Matrix calculateBoundingBox() {
        Rectangle rectangle = null;
        for (Position coordPosition : mCoordPositions) {
            if (rectangle == null) {
                rectangle = new Rectangle(new Point(coordPosition.getLongitude(), coordPosition.getLatitude()));
            } else {
                rectangle.add(new Point(coordPosition.getLongitude(), coordPosition.getLatitude()));
            }
        }
        return Matrix.zoomToFit(rectangle, new Rectangle((getWidth() - mDiameter) / 2, (getHeight() - mDiameter) / 2, mDiameter, mDiameter));
    }

    @Override
    public void componentResized(ComponentEvent _e) {
        int height = (int) _e.getComponent().getSize().getHeight();
        int width = (int) _e.getComponent().getSize().getWidth();

        if (height < width)
            mRadius = (height / 2) - 10;
        else
            mRadius = (width / 2) - 10;

        mDiameter = mRadius * 2;

        repaint();
    }

    @Override
    public void componentMoved(ComponentEvent _e) {

    }

    @Override
    public void componentShown(ComponentEvent _e) {

    }

    @Override
    public void componentHidden(ComponentEvent _e) {

    }
}
